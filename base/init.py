import tcod.event
import tcod
import time

MOVEMENT_KEYS = {
    # standard arrow keysconsole_init_root
    'UP': [0, -1],
    'DOWN': [0, 1],
    'LEFT': [-1, 0],
    'RIGHT': [1, 0],

    # diagonal keys
    # keep in mind that the keypad won't use these keys even if
    # num-lock is off
    'HOME': [-1, -1],
    'PAGEUP': [1, -1],
    'PAGEDOWN': [1, 1],
    'END': [-1, 1],

    # number-pad keys
    # These keys will always show as KPx regardless if num-lock
    # is on or off.  Keep in mind that some keyboards and laptops
    # may be missing a keypad entirely.
    # 7 8 9
    # 4   6
    # 1 2 3
    89: [-1, 1],
    90: [0, 1],
    91: [1, 1],
    92: [-1, 0],
    94: [1, 0],
    95: [-1, -1],
    96: [0, -1],
    97: [1, -1],
}

# Define the window size (in character tiles.) We'll pick something small.
WIDTH, HEIGHT = 50, 50  # 320x240 when the font size is taken into account

# Set the font to the example font in the tutorial folder.  This font is
# equivalent to the default font you will get if you skip this call.
# With the characters rows first and the font size included in the filename
# you won't have to specify any parameters other than the font file itself.
# tcod.console_set_custom_font('terminal8x8_gs_ro.png')
tcod.console_set_custom_font('./fonts/terminal12x12_gs_ro.png', tcod.FONT_LAYOUT_ASCII_INROW)
# Call tdl.init to create the root console.
# We will call drawing operations on the returned object.
console = tcod.console_init_root(WIDTH, HEIGHT, 'python-tdl tutorial', False, None, 'C')

playerX = 1
playerY = 1

keyPressedAlready = False
# Start an infinite loop.  Drawing and game logic will be put in this loop.
while True:

    # R#eset the console to a blank slate before drawing on it.
    console.clear()

    console.print(playerX, playerY, "@")
    # console.put_char(playerX, playerY, 40)

    key = 0
    flushArray = {
        "@1234567890-=+",
        "qwertyuiop[]{}",
        "asdfghjkl;:\'|",
        "zxcvbnm,./?<>\\",
        "!@#$%^&*()_+`~",
    }
    for string in flushArray:
        key += 1
        console.print(1, key, string)

    millis = str(round(time.time() * 1000))

    console.print(1, 12, millis)
    tcod.console_flush()
    # Using "(x, y) in console" we can quickly check if a position is inside of
    # a console.  And skip a draw operation that would otherwise fail.

    # TODO разобраться с эвентами
    # Handle events by iterating over the values returned by tdl.event.get
    for event in tcod.event.get():
        if event.type == 'KEYDOWN':
            # We mix special keys with normal characters so we use keychar.
            if event.scancode in MOVEMENT_KEYS:
                if keyPressedAlready:
                    keyPressedAlready = False
                    continue

                # Get the vector and unpack it into these two variables.
                keyX, keyY = MOVEMENT_KEYS[event.scancode]
                # Then we add the vector to the current player position.
                playerX += keyX
                playerY += keyY

                keyPressedAlready = True
        # Check if this is a 'QUIT' event
        if event.type == 'QUIT':
            # Later we may want to save the game or confirm if the user really
            # wants to quit but for now we break out of the loop by raising a
            # SystemExit exception.
            # The optional string parameter will be printed out on the
            # terminal after the script exits.
            raise SystemExit('The window has been closed.')
